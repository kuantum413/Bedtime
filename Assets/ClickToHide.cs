﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

public class ClickToHide : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void FixedUpdate()
    {
        if (CrossPlatformInputManager.GetButtonDown("Submit"))
        {
            gameObject.SetActive(false);
        }
    }
}
