﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class RecordedActions : MonoBehaviour
{
	/**
	 * Constants
     */
	public float X_SPACING = 40;
	public float Y_SPACING = 40;
	public int MAX_ROW_COUNT = 10;
	public string PREFAB_RESOURCE = "ActionPrefab";

	/**
	 * Member Variables
     */
	private float m_initXPosition;
    private float m_initYPosition;
	private float m_xPosition;
	private float m_yPosition;
	private int m_elementCount;
	private Hashtable m_actionElementsHash;

	/**
	 * Creates an action element and updates x, y position of next action element
     */
	public int createActionElement ()
	{
		GameObject uiAction =
			Instantiate (
				Resources.Load (PREFAB_RESOURCE),
				new Vector3 (m_xPosition, m_yPosition, 0),
				Quaternion.identity
			) as GameObject;

		// Assign x, y coordinates for next Item
		m_xPosition = m_xPosition + X_SPACING;

		m_actionElementsHash.Add (m_elementCount, uiAction);

		int elementKey = m_elementCount; 

		m_elementCount = m_elementCount + 1;

		if (m_elementCount % MAX_ROW_COUNT == 0) {
			m_yPosition = m_yPosition - Y_SPACING;
			m_xPosition = m_initXPosition;
		}

		uiAction.transform.SetParent (gameObject.transform, false);

		return elementKey;
	}

	/**
	 * Removes all action elements
	 */
	public void resetActionElements ()
	{
        m_xPosition = m_initXPosition;
        m_yPosition = m_initYPosition;
        m_elementCount = 0;
        m_actionElementsHash.Clear();
        GameObject actionElement = GameObject.Find("ActionPrefab(Clone)");
        while (null != actionElement)
        {
            DestroyImmediate(actionElement);
            actionElement = GameObject.Find("ActionPrefab(Clone)");
        }
    }

	/**
	 * Creates more than one action element
	 */
	public void createManyActionElements (int count)
	{
		for (int i = 0; i < count; i++) {
			createActionElement ();
		}
	}


	/**
	 * Changes the color of the element found at element key
	 */
	public void changeElementColor (int elementKey, Color color)
	{
		GameObject actionElement = getActionElement (elementKey);
		actionElement.GetComponent<UnityEngine.UI.Image> ().color = color;
	}

    // Change all element colors
    public void changeAllElementColors(Color color)
    {
        foreach (GameObject actionElement in m_actionElementsHash.Values)
        {
            actionElement.GetComponent<UnityEngine.UI.Image>().color = color;
        }
    }

	/**
	 * Returns the action element found at key element key
	 */
	public GameObject getActionElement (int elementKey)
	{
		return m_actionElementsHash [elementKey] as GameObject;
	}

	void Awake ()
	{
		m_xPosition = gameObject.transform.position.x;
		m_yPosition = gameObject.transform.position.y;
		m_initXPosition = m_xPosition;
        m_initYPosition = m_yPosition;
		m_elementCount = 0;
		m_actionElementsHash = new Hashtable ();
	}

	// Use this for initialization
	void Start ()
	{
        //This shows how you might use this script


        //int elementKey = createActionElement();
        //changeElementColor(elementKey, new Color(255, 0, 0));
        //createManyActionElements(19);
    }

	// Update is called once per frame
	void Update ()
	{
	}
}
