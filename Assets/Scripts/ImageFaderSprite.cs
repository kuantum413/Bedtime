﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ImageFaderSprite : MonoBehaviour
{
    public bool IS_FADING = false;
    public float FADE_SPEED = .1f;
    public GameObject m_ImageObject;
    private SpriteRenderer m_Sprite;

    // Use this for initialization
    void Start()
    {
        m_Sprite = m_ImageObject.GetComponent<SpriteRenderer>();
    }

    void Awake()
    {
    }

    private float getNextAlphaColor(float currentColor)
    {
        float nextAlphaColor;

        if (IS_FADING)
        {
            nextAlphaColor = currentColor - (Time.deltaTime * FADE_SPEED);
            if (nextAlphaColor < 0)
            {
                nextAlphaColor = 0;
            }
        }
        else {
            nextAlphaColor = currentColor + (Time.deltaTime * FADE_SPEED);
            if (nextAlphaColor > 1)
            {
                nextAlphaColor = 1;
            }
        }

        return nextAlphaColor;
    }

    // Update is called once per frame
    void Update()
    {
        Color currentColor = m_Sprite.color;

        float nextAlphaColor = getNextAlphaColor(currentColor.a);

        m_Sprite.color =
            new Color(
            currentColor.r,
            currentColor.g,
            currentColor.b,
            nextAlphaColor
        );
    }

    // Get alpha value
    public float getAlpha()
    {
        return m_Sprite.color.a;
    }
}