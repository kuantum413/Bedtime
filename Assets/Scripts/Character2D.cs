using System;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class Character2D : MonoBehaviour
    {
        [SerializeField] private float m_MaxSpeed = 10f;                    // The fastest the player can travel in the x axis.

        private Animator m_Anim;            // Reference to the player's animator component.
        private Rigidbody2D m_Rigidbody2D;
        private BoxCollider2D m_BoxCollider;
        private Vector2 m_Velocity;
        private Vector2 m_Direction;

        private void Awake()
        {
            // Setting up references.
            m_Anim = GetComponent<Animator>();
            m_Rigidbody2D = GetComponent<Rigidbody2D>();
            m_BoxCollider = GetComponent<BoxCollider2D>();
        }


        private void FixedUpdate()
        {
            // Set the direction and moving parameters to correspond with our new direction.
            if (m_Velocity.magnitude > 0.0F)
            {
                m_Anim.SetBool("Moving", true);
                if (Math.Abs(m_Velocity[1]) > Math.Abs(m_Velocity[0]))
                {
                    if (m_Velocity[1] > 0.0F)
                    {
                        m_Anim.SetInteger("Direction", 0);
                    }
                    else
                    {
                        m_Anim.SetInteger("Direction", 2);
                    }
                }
                else
                {
                    if (m_Velocity[0] > 0.0F)
                    {
                        m_Anim.SetInteger("Direction", 1);
                    }
                    else
                    {
                        m_Anim.SetInteger("Direction", 3);
                    }
                }
            }
            else
            {
                m_Anim.SetBool("Moving", false);
            }
        }


        public void Move(float moveH, float moveV)
        {

            // Move the character
            m_Velocity = new Vector2(moveH, moveV);
            m_Velocity = m_MaxSpeed * m_Velocity.normalized;
            m_Rigidbody2D.velocity = m_Velocity;
            if (m_Velocity.magnitude > 0.0F)
            {
                m_Direction = m_Velocity.normalized;
            }
        }

        public ObjectInteraction detect()
        {
            // Round our direction to the nearest multiple of 90 degrees and use it as our detection direction.
            float roundAngle = (float)Math.PI / 2.0F;
            float angle = (float)Math.Atan2(m_Direction[1], m_Direction[0]);
            Vector2 castDir;

            if (angle % roundAngle != 0)
            {
                float newAngle = (float)Math.Round(angle / roundAngle) * roundAngle;
                castDir = new Vector2((float)Math.Cos(newAngle), (float)Math.Sin(newAngle));
            }
            else
            {
                castDir = m_Direction;
            }

            // Cast our detection ray and see if an interactable object is in range.
            RaycastHit2D hit = Physics2D.Raycast(transform.position + transform.localScale[0] * (Vector3) m_BoxCollider.offset, castDir);
            if (hit && hit.distance < transform.localScale[0] *m_BoxCollider.size.magnitude)
            {
                ObjectInteraction objInteract = hit.transform.gameObject.GetComponent<ObjectInteraction>();
                if (objInteract != null)
                {
                   return objInteract;
                }
            }

            return null;
        }
    }
}
