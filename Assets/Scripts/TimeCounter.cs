﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TimeCounter : MonoBehaviour
{

    /**
     * Constants
     */
    public GameObject m_TextObject;
    public int INIT_HOUR = 11;
    public int INIT_MINUTE = 0;
    public bool INIT_IS_AM = false;
    public int TIMER_MULTIPLIER = 10;
    public string AM_TEXT = "AM";
    public string PM_TEXT = "PM";

    /**
     * Member Variables
     */
    private Text m_Text;
    private float m_minutes;
    private float m_hours;
    private bool m_isAM;
    private bool m_isTiming;

    /**
     * Resets time
     */
    public void resetTime()
    {
        setTime(INIT_HOUR, INIT_MINUTE, INIT_IS_AM);
    }

    /**
     * Sets the time to hours, minutes, and AM/PM
     */
    public void setTime(int hours, int minutes, bool isAM)
    {
        m_hours = (float)hours;
        m_minutes = (float)minutes;
        m_isAM = isAM;

        updateText();
    }

    /**
     * Stops counting
     */
    public void stopCounting()
    {
        m_isTiming = false;
    }

    /**
     * Starts counting
     */
    public void startCounting()
    {
        m_isTiming = true;
    }

    /**
     * Updates the text with the current stored time
     */
    private void updateText()
    {
        string hours = ((int)m_hours).ToString();
        string minutes = ((int)m_minutes).ToString();

        if (minutes.Length == 1)
        {
            minutes = "0" + minutes;
        }

        string timeText =
            hours +
            ":" +
            minutes +
            " ";

        if (m_isAM)
        {
            timeText += AM_TEXT;
        }
        else {
            timeText += PM_TEXT;
        }
        if (null != m_Text)
        {
            m_Text.text = timeText;
        }
    }

    /**
     * Toggles between day/night
     */
    private void toggleDayNight()
    {
        if (m_isAM)
        {
            m_isAM = false;
        }
        else {
            m_isAM = true;
        }
    }

    /**
     * Toggles between day/night
     */
    void Start()
    {
        m_Text = m_TextObject.GetComponent<Text>();
        setTime(INIT_HOUR, INIT_MINUTE, INIT_IS_AM);
        startCounting();
    }

    /**
     * Updates the timer every frame if the counter is timing
     */
    void Update()
    {
        if (m_isTiming)
        {
            m_minutes += (Time.deltaTime * TIMER_MULTIPLIER);

            if (m_minutes >= 60)
            {
                m_hours += 1;
                m_minutes = 0;

                if (m_hours == 12)
                {
                    toggleDayNight();
                }
                else if (m_hours > 12)
                {
                    m_hours = 1;
                }
            }

            updateText();
        }
    }

    public int getHour()
    {
        return (int) m_hours;
    }

    public bool isAM()
    {
        return m_isAM;
    }
}