using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets._2D
{
    [RequireComponent(typeof (Character2D))]
    public class UserControl2D : MonoBehaviour
    {
        private Character2D m_Character;
        private OCD m_OCD;
        private ObjectInteraction m_LastObject;
        private ImageFaderSprite m_InputSpriteFader;


        private void Awake()
        {
            m_Character = GetComponent<Character2D>();
            m_OCD = GetComponent<OCD>();
        }

        private void Start()
        {
            m_InputSpriteFader = GameObject.Find("InputPrompt").GetComponent<ImageFaderSprite>();
        }

        private void Update()
        {
            ObjectInteraction currentObject = m_Character.detect();
            if (currentObject == null) {
                m_InputSpriteFader.IS_FADING = true;
                if (m_LastObject != null)
                {
                    m_LastObject.setInteractable(false);
                    m_LastObject = null;
                }
            }
            else
            {
                m_InputSpriteFader.IS_FADING = false;
                if (m_LastObject != null && currentObject.GetInstanceID() != m_LastObject.GetInstanceID())
                {
                    m_LastObject.setInteractable(false);
                }
                currentObject.setInteractable(true);
                m_LastObject = currentObject;
            }

            // Check for user interaction.
            if (CrossPlatformInputManager.GetButtonDown("Submit") && currentObject != null)
            {
                m_OCD.interact(currentObject.GetInstanceID());
            }
        }


        private void FixedUpdate()
        {
            // Read the inputs.
            float h = CrossPlatformInputManager.GetAxis("Horizontal");
            float v = CrossPlatformInputManager.GetAxis("Vertical");
            // Pass all parameters to the character control script.
            m_Character.Move(h, v);
        }
    }
}
