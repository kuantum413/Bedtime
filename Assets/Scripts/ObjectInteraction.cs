﻿using UnityEngine;
using System.Collections;

public class ObjectInteraction : MonoBehaviour {

    private Animator m_Animator;
    private AudioSource m_Audio;

	// Use this for initialization
	void Start () {
        m_Animator = GetComponent<Animator>();
        m_Audio = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void setInteractable(bool interactable)
    {
        m_Animator.SetBool("Interactable", interactable);
    }

    public void setClean(bool clean)
    {
        if (null != m_Animator)
        {
            m_Animator.SetBool("Clean", clean);
        }
    }

    public void setDone(bool done)
    {
        if (tag == "Bed")
        {
            m_Animator.SetBool("Done", done);
        }
    }

    public void playSound()
    {
        m_Audio.Play();
    }
}
