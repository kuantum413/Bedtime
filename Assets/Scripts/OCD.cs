﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class OCD : MonoBehaviour {

    private RecordedActions m_RecordedActions;

    private List<int> m_ObjectIDs;
    private Dictionary<int, ObjectInteraction> m_Objects;
    private ObjectInteraction m_Bed;
    private ImageFader m_NightFade;
    private TimeCounter m_TimeCounter;

    private int m_Index;
    private List<int> m_Sequence;
    private bool m_NewSequence;
    private bool m_GameRunning;
    private bool m_Booting = true;

    // Relevant objects in the game.
    public GameObject m_StressOverlay;
    public AudioSource m_MistakeSound;
    public AudioSource m_VictorySound;
    public AudioSource m_LoseSound;

    // Use this for initialization.
    void Start () {
        m_RecordedActions = GameObject.Find("ProgressPanel").GetComponent<RecordedActions>();
        m_NightFade = GetComponent<ImageFader>();
        m_TimeCounter = GetComponent<TimeCounter>();
        m_ObjectIDs = new List<int>();
        m_Objects = new Dictionary<int, ObjectInteraction>();
        m_Sequence = new List<int>();
        m_NewSequence = false;

        // Grab all of the interaction objects in a scene.
        object[] objects = GameObject.FindObjectsOfType(typeof(GameObject));
        foreach (object o in objects)
        {
            GameObject gameObj = (GameObject)o;
            ObjectInteraction interactableObject = gameObj.GetComponent<ObjectInteraction>();
            if (interactableObject != null)
            {
                int ID = interactableObject.GetInstanceID();
                m_ObjectIDs.Add(ID);
                m_Objects.Add(ID, interactableObject);
                print(ID);

                if (interactableObject.tag == "Bed")
                {
                    m_Bed = interactableObject;
                }
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (m_Booting)
        {
            newGame();
            m_Booting = false;
        }

        if (m_TimeCounter.getHour() == 12 && m_TimeCounter.isAM() && !m_NewSequence)
        {
            m_NewSequence = true;
            m_LoseSound.Play();
            restartGame();
        }
	}

    // Generate a new random sequence of a given size.
    void generateNewRandomSequence(int size)
    {
        m_Index = 0;
        m_Sequence.Clear();

        for (int i = 0; i < size; i++)
        {
            int randomIndex = (int)Random.Range(0, m_ObjectIDs.Count - 0.001F);
            m_Sequence.Add(m_ObjectIDs[randomIndex]);
        }
    }

    // Generate a new random sequence of a given size.
    void incrememntSequence()
    {
        m_Index = 0;
        int randomIndex = (int)Random.Range(0, m_ObjectIDs.Count - 0.001F);
        m_Sequence.Add(m_ObjectIDs[randomIndex]);
    }

    // Check an action with an OCD object against the pre-defined sequence.
    public void interact(int ID)
    {
        // Do nothing unless the game is running.
        if (! m_GameRunning)
        {
            return;
        }

        // Grab the object in question if it exists.
        ObjectInteraction activeObject = m_Objects[ID];
        if (activeObject != null)
        {
            // Clean the object regardless of its status.
            activeObject.setClean(true);
            activeObject.playSound();

            if (activeObject.GetInstanceID() == m_Bed.GetInstanceID() && m_NewSequence)
            {
                m_VictorySound.Play();
                endLevel();
                return;
            }

            // If it is the right object in the sequence,
            if (m_Sequence[m_Index] == ID)
            {
                // Change the color in the UI.
                m_RecordedActions.changeElementColor(m_Index, new Color(255F / 255, 230F / 255, 90F / 255));

                // If it is the last object in the sequence,
                if (m_Index == m_Sequence.Count - 1)
                {
                    // Generate a new sequence, and reset all objects.
                    m_NewSequence = true;
                    m_Bed.setDone(true);
                    m_RecordedActions.changeAllElementColors(new Color(221F / 255, 67F / 255, 236F / 255));
                    return;
                }
                // Otherwise increment the index.
                else
                {
                    m_Index += 1;
                }
            }
            // If it is not the right object show the stress overlay.
            else
            {
                m_MistakeSound.Play();
                showStressOverlay(0.1F);
            }
            // Always check to see if the object needs to be re-dirtied.
            Invoke("resetRemainingObjects", 1);
        }
    }

    void restartGame()
    {
        m_GameRunning = false;
        m_NightFade.IS_FADING = false;
        m_TimeCounter.stopCounting();
        Invoke("newGame", 3);
    }

    void endLevel()
    {
        m_GameRunning = false;
        m_NightFade.IS_FADING = false;
        m_TimeCounter.stopCounting();
        Invoke("nextLevel", 3);
    }

    // Start the game anew;
    void newGame()
    {
        generateNewRandomSequence(3);
        m_NewSequence = false;
        m_Bed.setDone(false);

        startLevel();
    }

    // Start the next level.
    void nextLevel()
    {
        int length = m_Sequence.Count;
        if (m_Index == length - 1)
        {
            length++;
        }
        incrememntSequence();
        m_NewSequence = false;
        m_Bed.setDone(false);

        startLevel();
    }

    void startLevel()
    {
        m_RecordedActions.resetActionElements();
        m_RecordedActions.createManyActionElements(m_Sequence.Count);
        m_NightFade.IS_FADING = true;
        m_TimeCounter.resetTime();
        m_TimeCounter.startCounting();
        m_GameRunning = true;
        resetAllObjects();
    }

    // Reset all objects in the scene.
    void resetAllObjects()
    {
        foreach (ObjectInteraction obj in m_Objects.Values)
        {
            obj.setClean(true);
            if (m_Sequence.Contains(obj.GetInstanceID()))
            {
                obj.setClean(false);
            }
        }
    }

    // Reset an object if it is still in the list.
    void resetRemainingObjects()
    {
        foreach (int ID in m_Objects.Keys)
        {
            if (remainsInSequence(ID))
            {
                ObjectInteraction resetObject = m_Objects[ID];
                resetObject.setClean(false);
            }
        }
    }

    // See if a given ID remains in the sequence.
    bool remainsInSequence(int ID)
    {
        for (int i = m_Index; i < m_Sequence.Count; i++)
        {
            if (m_Sequence[i] == ID)
            {
                return true;
            }
        }
        return false;
    }

    // Flash the stress overlay for a few seconds.
    void showStressOverlay(float seconds)
    {
        m_StressOverlay.SetActive(true);
        Invoke("hideStressOverlay", seconds);
    }

    // Hide the stree overlay.
    void hideStressOverlay()
    {
        m_StressOverlay.SetActive(false);
    }
}
